#!/bin/bash

composer install

while :
do
    (echo > /dev/tcp/mysql/3306) >/dev/null 2>&1
    WAITFORIT_result=$?
    if [[ $WAITFORIT_result -eq 0 ]]; then
        WAITFORIT_end_ts=$(date +%s)
        php /var/www/html/email-sender/bin/console doctrine:migrations:migrate --no-interaction --allow-no-migration
        break
    fi
    sleep 1
done

php-fpm


