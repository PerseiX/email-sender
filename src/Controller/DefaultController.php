<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Manager\ManagerInterface;
use App\Form\Type\PersonType;

/**
 * Class DefaultController
 * @package App\Controller
 */
class DefaultController extends AbstractController
{
    /**
     * @var ManagerInterface
     */
    protected $manager;

    /**
     * DefaultController constructor.
     * @param ManagerInterface $manager
     */
    public function __construct(ManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $form = $this->createForm(PersonType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->send($form->getData());

            $this->addFlash('success', 'Pomyślnie przesłano formularz');

            return $this->redirectToRoute('index');
        }

        return $this->render('default/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
