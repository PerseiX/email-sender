<?php


namespace App\Service;


use App\Model\FormDataInterface;
use App\Model\Person;

class SendEmail implements PropagateDataInterface
{

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * SendEmail constructor.
     * @param \Swift_Mailer $mailer
     */
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param FormDataInterface $model
     */
    public function send(FormDataInterface $model): void
    {
        /** @var Person $model */
        $message = (new \Swift_Message('Hello Email'))
            ->setFrom($model->getEmail())
            ->setTo($model->getEmail())
            ->setBody(sprintf("Witaj %s %s! Co u Ciebie? Masz całe %s lat!", $model->getName(), $model->getSurname(), $model->getAge()));

       $this->mailer->send($message);
    }
}
