<?php


namespace App\Service;


use App\Model\FormDataInterface;

interface PropagateDataInterface
{
    public function send(FormDataInterface $propagateData): void;
}
