<?php


namespace App\Consumer;


use App\Model\Person;
use App\Service\PropagateDataInterface;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class TaskConsumer implements ConsumerInterface
{

    /**
     * @var Serializer
     */
    private $jmsSerializer;

    /**
     * @var PropagateDataInterface
     */
    private $sendEmail;

    /**
     * TaskConsumer constructor.
     * @param SerializerInterface $jmsSerializer
     * @param PropagateDataInterface $sendEmail
     */
    public function __construct(SerializerInterface $jmsSerializer, PropagateDataInterface $sendEmail)
    {
        $this->jmsSerializer = $jmsSerializer;
        $this->sendEmail = $sendEmail;
    }


    /**
     * @param AMQPMessage $msg
     * @return mixed|void
     */
    public function execute(AMQPMessage $msg)
    {
        $person = $this->jmsSerializer->deserialize($msg->getBody(), Person::class, 'json');
        $this->sendEmail->send($person);
    }

}
