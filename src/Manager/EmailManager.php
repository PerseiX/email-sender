<?php


namespace App\Manager;


use JMS\Serializer\SerializerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use App\Model\FormDataInterface;
use App\Model\Person;

class EmailManager implements ManagerInterface
{

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var ProducerInterface
     */
    private $producer;

    /**
     * EmailManager constructor.
     * @param SerializerInterface $jmsSerializer
     * @param ProducerInterface $producer
     */
    public function __construct(SerializerInterface $jmsSerializer, ProducerInterface $producer)
    {
        $this->serializer = $jmsSerializer;
        $this->producer = $producer;
    }


    /**
     * @param FormDataInterface $formData
     * @return mixed
     */
    public function send(FormDataInterface $formData): void
    {
        $serializedData = $this->serializer->serialize($formData, 'json', null, Person::class);
        $this->producer->publish($serializedData);
    }
}
