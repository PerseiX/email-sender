<?php


namespace App\Manager;


use App\Model\FormDataInterface;

interface ManagerInterface
{
    /**
     * @param FormDataInterface $formData
     * @return mixed
     */
    public function send(FormDataInterface $formData): void;
}
