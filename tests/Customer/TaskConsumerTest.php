<?php


namespace App\Tests\Consumer;


use App\Consumer\TaskConsumer;
use App\Model\Person;
use App\Service\PropagateDataInterface;
use JMS\Serializer\SerializerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class TaskConsumerTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $serializer;

    /**
     * @var MockObject
     */
    private $sendEmail;

    /**
     * @var TaskConsumer
     */
    private $consumer;

    /**
     * @throws \ReflectionException
     */
    public function setUp(): void
    {
        $this->sendEmail = $this->createMock(PropagateDataInterface::class);
        $this->serializer = $this->createMock(SerializerInterface::class);
        $this->consumer = new TaskConsumer($this->serializer, $this->sendEmail);
    }


    public function testShouldPropagateData(): void
    {
        $this->serializer
            ->method('deserialize')
            ->willReturn(new Person());

        $this->sendEmail
            ->expects($this->once())
            ->method('send')
            ->will($this->returnValue(1));


        $message = new AMQPMessage('{ "name": "Kamil", "surname": "Rytel", "age": 13, "email": "email@test.pl" }');
        $this->consumer->execute($message);

    }
}
