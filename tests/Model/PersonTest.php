<?php

namespace App\Tests\Model;

use App\Model\Person;
use PHPUnit\Framework\TestCase;

class PersonTest extends TestCase
{
    public function testShouldModelBasic()
    {
        $person = new Person();
        $person->setAge(123);
        $person->setEmail("kamil@test.pl");
        $person->setName("kamil");
        $person->setSurname("rytel");

        $this->assertEquals(123, $person->getAge());
        $this->assertEquals("kamil@test.pl", $person->getEmail());
        $this->assertEquals("kamil", $person->getName());
        $this->assertEquals("rytel", $person->getSurname());
    }
}
