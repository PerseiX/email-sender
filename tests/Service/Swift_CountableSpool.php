<?php

namespace App\Tests\Service;

use Swift_MemorySpool;

class Swift_CountableSpool extends Swift_MemorySpool
{
    /**
     * @return array
     */
    public function getMessages(): array
    {
        return $this->messages;
    }
}
