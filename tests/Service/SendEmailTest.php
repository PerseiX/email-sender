<?php


namespace App\Service;


use App\Model\Person;
use App\Tests\Service\Swift_CountableSpool;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class SendEmailTest extends TestCase
{


    /**
     * @var Swift_CountableSpool
     */
    private $spool;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;


    public function setUp(): void
    {
        $this->spool = new Swift_CountableSpool();
        $transport = new \Swift_SpoolTransport($this->spool);
        $this->mailer= new \Swift_Mailer($transport);
    }


    /**
     * @return array
     */
    public function personProvider()
    {
        $person = new Person();
        $person
            ->setName("Kamil")
            ->setSurname("Rytel")
            ->setEmail("kamil@test.pl")
            ->setAge(10);

        return [[$person, "Witaj Kamil Rytel! Co u Ciebie? Masz całe 10 lat!"]];
    }

    /**
     * @dataProvider personProvider
     *
     * @param Person $person
     * @param $expectMessage
     */
    public function testShouldSpoolEmailsSend(Person $person, $expectMessage)
    {
        $sendEmail = new SendEmail($this->mailer);
        $sendEmail->send($person);

        /** @var \Swift_Message $message */
        $messages = $this->spool->getMessages();
        $message = $messages[0] ?? null;
        $this->assertEquals($expectMessage, $message->getBody());
        $this->assertCount(1, $this->spool->messages);
    }

}
