<?php

namespace App\Tests\Manager;

use App\Manager\EmailManager;
use App\Model\Person;
use JMS\Serializer\SerializerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class EmailManagerTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $serializer;

    /**
     * @var MockObject
     */
    private $producer;

    /**
     * @var EmailManager
     */
    private $emailManager;

    /**
     * @throws \ReflectionException
     */
    protected function setUp(): void
    {
        $this->serializer = $this->createMock(SerializerInterface::class);
        $this->producer = $this->createMock(ProducerInterface::class);
        $this->emailManager = new EmailManager($this->serializer, $this->producer);
    }

    /**
     * @dataProvider personProvider
     * @param string $personJson
     * @param Person $person
     *
     * @throws \ReflectionException
     */
    public function testShouldSimulatePublishData(string $personJson, Person $person)
    {
        $this->serializer
            ->expects($this->once())
            ->method('serialize')
            ->willReturn($personJson);


        $this->producer
            ->expects($this->once())
            ->method('publish')
            ->with($personJson);

        $this->emailManager->send($person);
    }

    /**
     * @return array
     */
    public function personProvider(): array
    {
        $person = new Person();
        $person
            ->setAge(10)
            ->setEmail("jan.kowalski@o2.pl")
            ->setSurname("Kowalski")
            ->setName("Jan");

        return [['{ "name": "Jan", "surname": "Kowalski", "age": 10, "email": "jan.kowalski@o2.pl" }', $person]];
    }
}
