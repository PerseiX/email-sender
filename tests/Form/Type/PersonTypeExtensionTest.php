<?php

namespace App\Tests\Form\Type;


use App\Form\Type\PersonType;
use App\Model\Person;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Test\TypeTestCase;

class PersonTypeExtensionTest extends TypeTestCase
{
    /**
     * @var FormInterface
     */
    private $form;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->form = $this->factory->create(PersonType::class);
    }

    /**
     * @dataProvider formDataProvider
     * @param array $formData
     */
    public function testShouldCheckSuccessSubmission(array $formData): void
    {
        $person = new Person();
        $person
            ->setName('kamil')
            ->setSurname('rytel')
            ->setEmail('test@o2.pl')
            ->setAge(123);

        $this->form->submit($formData);

        $this->assertTrue($this->form->isSynchronized());
        $this->assertEquals($person, $this->form->getData());

    }

    /**
     * @dataProvider formDataProvider
     * @param array $formData
     */
    public function testShouldFormContainInputs(array $formData): void
    {
        $view = $this->form->createView();
        $children = $view->children;


        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }

    public function formDataProvider(): array
    {
        return [[[
            'name' => 'kamil',
            'surname' => 'rytel',
            'email' => 'test@o2.pl',
            'age' => 123,
        ]]];
    }
}
